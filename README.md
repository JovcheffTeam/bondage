# Bondage

[![CI Status](http://img.shields.io/travis/yovchev03@gmail.com/Bondage.svg?style=flat)](https://travis-ci.org/yovchev03@gmail.com/Bondage)
[![Version](https://img.shields.io/cocoapods/v/Bondage.svg?style=flat)](http://cocoapods.org/pods/Bondage)
[![License](https://img.shields.io/cocoapods/l/Bondage.svg?style=flat)](http://cocoapods.org/pods/Bondage)
[![Platform](https://img.shields.io/cocoapods/p/Bondage.svg?style=flat)](http://cocoapods.org/pods/Bondage)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

Bondage is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "Bondage"
```

## Author

yovchev03@gmail.com, yovchev03@gmail.com

## License

Bondage is available under the MIT license. See the LICENSE file for more info.
